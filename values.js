function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

    let result = [];

    for(let key in obj) {

        result.push(obj[key]);

    }

    return result;
}

module.exports = values;