function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs

    let list = [];

    for(let key in obj) {

        let pair = [];
        pair.push(key);
        pair.push(obj[key]);

        list.push(pair);
    }

    return list;
}

module.exports = pairs;