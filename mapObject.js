function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    let newObj = {};

    for(let key in obj){

        let value = cb(obj[key], key);

        newObj[key] = value;

    }

    return newObj;
}

module.exports = mapObject;